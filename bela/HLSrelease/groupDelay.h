/// Gammatone filter group delay
///
/// Calculates the average group delay of a gammatone filter centered at *fc*. More precisely, it is the delay of the impulse response envelope peak.
/// Based on *Yang, Y., Jiang, Y., Liu, R., & Li, D. (2015). A realtime analysis/synthesis Gammatone filterbank. In 2015 IEEE International Conference on Signal Processing, Communications and Computing, ICSPCC 2015. Institute of Electrical and Electronics Engineers Inc. https://doi.org/10.1109/ICSPCC.2015.7338847*.
/// @param fc Center frequency of gammtone filter.
/// @returns Delay of envelope peak [s].
/// @note The order of the gammatone filter in this implementation is fixed to four.

float groupDelay(float fc);