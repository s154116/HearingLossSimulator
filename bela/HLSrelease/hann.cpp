/***** hann.cpp *****/

#include <hann.h>
#include <vector>
#include <math.h>

// Generates N point hann window


std::vector<float> hann(float L) {
	std::vector<float> w(L);
	int N = L - 1;
	for (int i = 0; i < L; i++) {
		w[i] = 0.5*(1 - cos(2*M_PI*i/N));
	}
	return w;
}