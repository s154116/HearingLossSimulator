// Function that calculates gain of variabel length SOS chain
#include <SOSgain.h>
#include <complex>
#include <algorithm>

float SOSgain(std::vector<SOS> filters, float fr, float fs, int n) {
	// Number of cascaded SOS objects
	int L = filters.size();
	
	// Resolution of search vector
	int N = 1000;
	
	std::vector<std::complex<double>> f(N);
	
	// Make linspace search frequency vector
	float flow = fr*2/3;
	float fhigh = fr*1;
	float fstep = (fhigh - flow)/N;
	for (int i = 0; i < N; i++) {
		f[i] = std::complex<double>(-1*(flow + ((double)i + 1.0)*fstep)/(fs/2), 0.0);
	}
	
	// Find gain of biquad cascade at that frequency
	std::complex<double> tmpGain;
	// Complex unit
	std::complex<double> I(0.0, 1.0);
	// pi
	std::complex<double> pi((double)M_PI, 0.0);
	// Individual filter gain
	std::vector<std::vector<std::complex<float>>> gainList(L, std::vector<std::complex<float>>(N, std::complex<float>(0.0, 0.0)));;
	// Full system gain
	std::vector<double> sysGain(N, 0.0);
	
	// Initialize complex coefficients
	std::complex<double> b00;
	std::complex<double> b01;
	std::complex<double> b02;
	std::complex<double> b10;
	std::complex<double> b11;
	std::complex<double> b12;
	std::complex<double> b20;
	std::complex<double> b21;
	std::complex<double> b22;
	std::complex<double> b30;
	std::complex<double> b31;
	std::complex<double> b32;
	std::complex<double> a00;
	std::complex<double> a01;
	std::complex<double> a02;
	std::complex<double> a10;
	std::complex<double> a11;
	std::complex<double> a12;
	std::complex<double> a20;
	std::complex<double> a21;
	std::complex<double> a22;
	std::complex<double> a30;
	std::complex<double> a31;
	std::complex<double> a32;

	// Loop over SOS
	for (int i = 0; i < L; i++) {
		// Cast biquad coefficients to complex
		b00 = std::complex<double>(filters[i].QuadBiquadSeries[0]->filters[n].b0, 0.0);
		b01 = std::complex<double>(filters[i].QuadBiquadSeries[0]->filters[n].b1, 0.0);
		b02 = std::complex<double>(filters[i].QuadBiquadSeries[0]->filters[n].b2, 0.0);
		b10 = std::complex<double>(filters[i].QuadBiquadSeries[1]->filters[n].b0, 0.0);
		b11 = std::complex<double>(filters[i].QuadBiquadSeries[1]->filters[n].b1, 0.0);
		b12 = std::complex<double>(filters[i].QuadBiquadSeries[1]->filters[n].b2, 0.0);
		b20 = std::complex<double>(filters[i].QuadBiquadSeries[2]->filters[n].b0, 0.0);
		b21 = std::complex<double>(filters[i].QuadBiquadSeries[2]->filters[n].b1, 0.0);
		b22 = std::complex<double>(filters[i].QuadBiquadSeries[2]->filters[n].b2, 0.0);
		b30 = std::complex<double>(filters[i].QuadBiquadSeries[3]->filters[n].b0, 0.0);
		b31 = std::complex<double>(filters[i].QuadBiquadSeries[3]->filters[n].b1, 0.0);
		b32 = std::complex<double>(filters[i].QuadBiquadSeries[3]->filters[n].b2, 0.0);
		
		a00 = std::complex<double>(filters[i].QuadBiquadSeries[0]->filters[n].a0, 0.0);
		a01 = std::complex<double>(filters[i].QuadBiquadSeries[0]->filters[n].a1, 0.0);
		a02 = std::complex<double>(filters[i].QuadBiquadSeries[0]->filters[n].a2, 0.0);
		a10 = std::complex<double>(filters[i].QuadBiquadSeries[1]->filters[n].a0, 0.0);
		a11 = std::complex<double>(filters[i].QuadBiquadSeries[1]->filters[n].a1, 0.0);
		a12 = std::complex<double>(filters[i].QuadBiquadSeries[1]->filters[n].a2, 0.0);
		a20 = std::complex<double>(filters[i].QuadBiquadSeries[2]->filters[n].a0, 0.0);
		a21 = std::complex<double>(filters[i].QuadBiquadSeries[2]->filters[n].a1, 0.0);
		a22 = std::complex<double>(filters[i].QuadBiquadSeries[2]->filters[n].a2, 0.0);
		a30 = std::complex<double>(filters[i].QuadBiquadSeries[3]->filters[n].a0, 0.0);
		a31 = std::complex<double>(filters[i].QuadBiquadSeries[3]->filters[n].a1, 0.0);
		a32 = std::complex<double>(filters[i].QuadBiquadSeries[3]->filters[n].a2, 0.0);
		
		for (int j = 0; j < N; j++) {
			tmpGain = ((b00 + b01*exp(pi*I*f[j]) + b02*exp(pi*I*f[j]*2.))
					*pow(a00 + a01*exp(pi*I*f[j]) + a02*exp(pi*I*f[j]*2.), -1.0)
						
				*  (b10 + b11*exp(pi*I*f[j]) + b12*exp(pi*I*f[j]*2.))
					*pow(a10 + a11*exp(pi*I*f[j]) + a12*exp(pi*I*f[j]*2.), -1.0)
						
				*  (b20 + b21*exp(pi*I*f[j]) + b22*exp(pi*I*f[j]*2.))
					*pow(a20 + a21*exp(pi*I*f[j]) + a22*exp(pi*I*f[j]*2.), -1.0)
						
				*  (b30 + b31*exp(pi*I*f[j]) + b32*exp(pi*I*f[j]*2.))
					*pow(a30 + a31*exp(pi*I*f[j]) + a32*exp(pi*I*f[j]*2.), -1.0));

			gainList[i][j] = tmpGain;
		}
	}
	
	for (int i = 0; i < N; i++) {
		tmpGain = gainList[0][i];
		for (int j = 1; j < L; j++) {
			tmpGain *= gainList[j][i];
		}
		sysGain[i] = fabs(tmpGain);
	}

	// Find and return maximal gain in linear units
	return *std::max_element(sysGain.begin(), sysGain.end());
}



