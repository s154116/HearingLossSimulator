#include <vector>

/// Frequency Response 
///
/// This function calculates the magnitude response of a set of four cascaded direct form II biquad IIR filters at uniformaly sampled frequency bins. 
/// The frequency response is one sided with 0Hz and fs as symmetry points.
/// @param[in] coeffs A [4x6] two-dimensional vector with filter coefficients. Filters are direct form II biquads.
/// @param[in] fs System samping frequency.
/// @param[in] N Number of frequency sampling points. Govers spectral resolution.
/// @param[out] Output is [N] dimensional vector containing magnitude response at sampling points.


std::vector<float> auditoryFilter(std::vector<std::vector<float>> coeffs, float fs, int N);