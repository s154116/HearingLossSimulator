/***** unityBuild.cpp *****/

#include <SOS.h>
#include <filterbank.h>
#include <Bela.h>

SOS::SOS() {
	for (int i = 0; i < 4; i++) {
		QuadBiquad* tmp = new QuadBiquad;
		QuadBiquadSeries.push_back(tmp);
	}
}


SOS::SOS(std::vector<std::vector<std::vector<float>>> initCoeffs) {
//	std::vector<QuadBiquad*> QuadBiquadSeries;
	for (int i = 0; i < 4; i++) {
		QuadBiquad* tmp = new QuadBiquad;
		QuadBiquadSeries.push_back(tmp);
	}
	updateCoeffs(initCoeffs);
	for (int i = 0; i < 4; i++) {
		QuadBiquadSeries[i]->update();
	}
}

void SOS::process(float data[4]) {
	for (int i = 0; i < 4; i++) {
		QuadBiquadSeries[i]->process(data);
	}
}

void SOS::reset() {
	return;
}

void SOS::update() {
	for (int i = 0; i < 4; i++) {
		QuadBiquadSeries[i]->update();
	}
}

void SOS::updateCoeffs(int n, std::vector<std::vector<float>> &newCoeffs) {
	for (int i = 0; i < 4; i++) {
		QuadBiquadSeries[i]->filters[n].b0 = newCoeffs[i][0];
		QuadBiquadSeries[i]->filters[n].b1 = newCoeffs[i][1];
		QuadBiquadSeries[i]->filters[n].b2 = newCoeffs[i][2];
		QuadBiquadSeries[i]->filters[n].a0 = newCoeffs[i][3];
		QuadBiquadSeries[i]->filters[n].a1 = newCoeffs[i][4];
		QuadBiquadSeries[i]->filters[n].a2 = newCoeffs[i][5];
		
		QuadBiquadSeries[i]->update();
	}
}

void SOS::updateCoeffs(std::vector<std::vector<std::vector<float>>> &newCoeffs) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			QuadBiquadSeries[i]->filters[j].b0 = newCoeffs[j][i][0];
			QuadBiquadSeries[i]->filters[j].b1 = newCoeffs[j][i][1];
			QuadBiquadSeries[i]->filters[j].b2 = newCoeffs[j][i][2];
			QuadBiquadSeries[i]->filters[j].a0 = newCoeffs[j][i][3];
			QuadBiquadSeries[i]->filters[j].a1 = newCoeffs[j][i][4];
			QuadBiquadSeries[i]->filters[j].a2 = newCoeffs[j][i][5];
		}
		QuadBiquadSeries[i]->update();
	}
}

filterbank::filterbank() {
	bank = std::vector<SOS>(0);
	Nch = 0;
	Nsos = 0;
}

// Copy constructor
filterbank::filterbank(const filterbank &fb) {
	Nch = fb.Nch;
	Nsos = fb.Nsos;
	bank = std::vector<SOS>(Nsos);
	
	std::vector<std::vector<std::vector<float>>> tmpCoeffs;
	tmpCoeffs.resize(4, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	
	for (int i = 0; i < Nsos; i++) {
		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 4; k++) {
				tmpCoeffs[j][k][0] = fb.bank[i].QuadBiquadSeries[k]->filters[j].b0;
				tmpCoeffs[j][k][1] = fb.bank[i].QuadBiquadSeries[k]->filters[j].b1;
				tmpCoeffs[j][k][2] = fb.bank[i].QuadBiquadSeries[k]->filters[j].b2;
				tmpCoeffs[j][k][3] = fb.bank[i].QuadBiquadSeries[k]->filters[j].a0;
				tmpCoeffs[j][k][4] = fb.bank[i].QuadBiquadSeries[k]->filters[j].a1;
				tmpCoeffs[j][k][5] = fb.bank[i].QuadBiquadSeries[k]->filters[j].a2;
			}
		}
		bank[i].updateCoeffs(tmpCoeffs);
	}
}


filterbank::filterbank(int N, std::vector<std::vector<std::vector<float>>> &initCoeffs) {
	Nch = N;
	Nsos = N / 4;
	bank = std::vector<SOS>(Nsos);

	std::vector<std::vector<std::vector<float>>> tmpCoeffs(4, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.)));
	
	for (int i = 0; i < Nsos; i++) {
		for (int j = 0; j < 4; j++) {
			tmpCoeffs[j] = initCoeffs[i*4 + j];
		}
		bank[i].updateCoeffs(tmpCoeffs);
	}
	return;
}

void filterbank::updateCoeffs(int n, std::vector<std::vector<float>> &newCoeffs) {
	int iSOS = n / 4;
	int iBq = n % 4;
	bank[iSOS].updateCoeffs(iBq, newCoeffs);
}

void filterbank::reset() {
	for (int i = 0; i < Nsos; i++) {
		bank[i].reset();
	}
}

std::vector<float> filterbank::process(std::vector<float> &input) {
	std::vector<float> output(Nch);
	float data[4];
	for (int i = 0; i < Nsos; i++) {
		for (int j = 0; j < 4; j++) {
			data[j] = input[i*4 + j];
		}
		bank[i].process(data);
		for (int j = 0; j < 4; j++) {
			output[i*4 + j] = data[j];
		}
	}
	return output;
}

std::vector<float> filterbank::process(float input) {
	std::vector<float> output(Nch);
	float data[4];
	for (int i = 0; i < Nsos; i++) {
		for (int j = 0; j < 4; j++) {
			data[j] = input;
		}
		bank[i].process(data);
		for (int j = 0; j < 4; j++) {
			output[i*4 + j] = data[j];
		}
	}
	return output;
}

