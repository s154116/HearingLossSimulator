#include <HLaudiogram.h>
#include <algorithm>

std::vector<float> HLaudiogram(std::vector<float> freqs, std::string type) {
	// Uses audiograms from Nikolai Bisgaard1, Marcel S. M. G. Vlaming2, and Martin Dahlquist
	// "Extrapolation" outside data range is done by assigning boundary values, thus assuming flat response
	
	// Location of datapoints
	std::vector<float> dataFreqs = {250, 375, 500, 750, 1000, 1500, 2000, 3000, 4000, 6000};
	std::vector<float> audiogram;
	
	// Data
	if (type.compare("N2") == 0) {
		audiogram = std::vector<float>({20, 20, 20, 22.5, 25, 30, 35, 40, 45, 50});
	}
	if (type.compare("N4") == 0) {
		audiogram = std::vector<float>({55, 55, 55, 55, 55, 60, 65, 70, 75, 80});
	}
	if (type.compare("N6") == 0) {
		audiogram = std::vector<float>({75, 77.5, 80, 82.5, 85, 90, 90, 95, 100, 100});
	}
	if (type.compare("S1") == 0) {
		audiogram = std::vector<float>({10, 10, 10, 10, 10, 10, 15, 30, 55, 70});
	}
	if (type.compare("S2") == 0) {
		audiogram = std::vector<float>({20, 20, 20, 22.5, 25, 35, 55, 75, 95, 95});
	}
	if (type.compare("S3") == 0) {
		audiogram = std::vector<float>({30, 30, 35, 47.5, 60, 70, 75, 80, 80, 85});
	}
	if (type.compare("None") == 0) {
		audiogram = std::vector<float>({0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
	}
	
	// Interpolation onto freqs
	std::vector<float> HLgains(freqs.size());
	for (int i = 0; i < freqs.size(); i++) {
		if (freqs[i] < dataFreqs[0]) {
			HLgains[i] = audiogram[0];	
		}
		else if (freqs[i] > dataFreqs[9]) {
			HLgains[i] = audiogram[9];
		}
		else {
			// Interpolate
			auto above = std::upper_bound(dataFreqs.begin(), dataFreqs.end(), freqs[i]);
			auto below = above - 1;
			float fhigh = *above;
			float flow = *below;
			float gainHigh = audiogram[above - dataFreqs.begin()];
			float gainLow = audiogram[below - dataFreqs.begin()];
			// Slope
			float m = (gainHigh - gainLow) / (fhigh - flow);
			HLgains[i] = gainLow + m*(freqs[i] - flow);
		}
		
	}
	
	return HLgains;
}


