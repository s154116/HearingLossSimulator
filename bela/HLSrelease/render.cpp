/**
@file render.cpp
*/

// Own libraries
#include <gammaTone.h>
#include <erbSpace.h>
#include <AF.h>
#include <filterbank.h>
#include <SOSgain.h>
#include <FIFO.h>
#include <groupDelay.h>
#include <hann.h>
#include <levelEstimator.h>
#include <findClosest.h>
#include <printCoeffs.h>
#include <auditoryFilter.h>
#include <HLaudiogram.h>

// External libraries
#include <algorithm> 
#include <libraries/Gui/Gui.h>
#include <libraries/GuiController/GuiController.h>
#include <libraries/Oscillator/Oscillator.h>
#include <Bela.h>
#include <vector>
#include <libraries/Scope/Scope.h>
#include <vector>
#include <array>
#include <ownFFT.h>
#include <chrono>
#include <libraries/WriteFile/WriteFile.h>
#include <Core>
#include <iostream>
#include <string>
#include "MonoFilePlayer.h"

// Welcome to the Hearing Loss Simulator
int N = 20; 					// Number of frequency channels - must be integer mulitple of 4!
int NFFT = 128;
float C = 85 + 6;				// Calibration
int ds = 2; 				// Downsampling factor
int f_low = 100;
int f_high = 8000;

// Scope
Scope scope;

// GUI sliders
Gui gui;
GuiController controller;
float outGain; 
float inpGain;
float tmpInpGain;
float inpCal;
float SmearingB;
int HLType;
int invCompTrig;


// Implementation parameters
int Nlevels = 100;			// Number of precomputed levels
int idx;					// Index for Pc search
int blockSize;				// Downsampled audio block size
float Pc;					// Power

// Model parameters (from Irino & Patterson)
float b1 = 1.81;
float c1 = -2.96;
float b2 = 2.17;
float c2 = 2.2;
float fratL = 1.08;
float fs;

// Level, frequency and signal vectors
std::vector<float> tmpVec(N);
float inp;
std::vector<std::vector<std::vector<std::vector<float>>>> hpafDatabase(Nlevels, std::vector<std::vector<std::vector<float>>>(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0))));
std::vector<std::vector<std::vector<std::vector<float>>>> invhpafDatabase(Nlevels, std::vector<std::vector<std::vector<float>>>(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0))));
std::vector<std::vector<std::vector<std::vector<float>>>> invCompDatabase(Nlevels, std::vector<std::vector<std::vector<float>>>(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0))));
std::vector<float> freqs = erbSpace(f_low, f_high, N);
std::vector<float> levels(Nlevels);
std::vector<std::vector<float>> s1;
std::vector<std::vector<float>> s2;
std::vector<float> s1rms(N);
std::vector<float> s2rms(N);
std::vector<std::vector<float>> gtOut;
std::vector<std::vector<float>> dhpaf0Out;
std::vector<std::vector<float>> dhpaf1Out;
std::vector<std::vector<float>> invlpaf0Out;
std::vector<std::vector<float>> invlpaf1Out;
std::vector<std::vector<float>> invdhpaf0Out;
std::vector<std::vector<float>> invdhpaf1Out;
std::vector<std::vector<float>> invComp0Out;
std::vector<std::vector<float>> invComp1Out;
std::vector<std::vector<float>> chain0Out;
std::vector<std::vector<float>> chain1Out;
std::vector<float> signals(N);
std::vector<float> audioOut;
std::vector<float> oaOut;
std::vector<float> inpBuf;


// Filterbank objects
filterbank gtFB;
filterbank lpafFB;
filterbank shpafFB;
filterbank dhpafFB_0;
filterbank dhpafFB_1;
filterbank invlpafFB_0;
filterbank invlpafFB_1;
filterbank invdhpafFB_0;
filterbank invdhpafFB_1;
filterbank invCompFB_0;
filterbank invCompFB_1;

// Estimators
std::vector<levelEstimator> estimators;

// Coefficient vectors
std::vector<std::vector<std::vector<float>>> newCoeffs(4, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
std::vector<std::vector<std::vector<float>>> newInvCoeffs(4, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
std::vector<std::vector<std::vector<float>>> newInvCompCoeffs(4, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));

// Hann window
std::vector<float> hannW;

// Overlap-add switch
bool oa_switch = false;

// Memory of previous Pc
std::vector<float> PcMem(N, 0.);

// Audio block count
int trig = 0;

// FIFO
std::vector<FIFO> SynthFIFO(N); 			// FIFO group delay compensation buffers
std::vector<std::vector<float>> FIFOin; 	// Buffer for FIFO synthesis input
std::vector<std::vector<float>> FIFOout;	// Buffer for FIFO synthesis output

// FFT
int ZP = 1; // Zero padding factor
std::vector<float> fftinp_0; // Input buffer
std::vector<float> fftinp_1; // Input buffer
std::vector<float> fft_oaBuf; // Overlapping buffer
std::vector<float> fftout_0; // Output buffer
std::vector<float> fftout_1; // Output buffer
std::vector<float> fftHann; // Hann window
std::vector<float> oa_Buf;

// FFT objects
Fft fft_0(NFFT*ZP);
Fft fft_1(NFFT*ZP);

// Eigen Matrices
int FFThalf = NFFT*ZP/2 + 1;
Eigen::MatrixXf A_1 = Eigen::MatrixXf::Zero(FFThalf, FFThalf);
Eigen::MatrixXf A_3 = Eigen::MatrixXf::Zero(FFThalf, FFThalf);
Eigen::MatrixXf A_6 = Eigen::MatrixXf::Zero(FFThalf, FFThalf);
Eigen::VectorXf v_0(FFThalf);
Eigen::VectorXf v_1(FFThalf);
Eigen::VectorXf out_0(FFThalf);
Eigen::VectorXf out_1(FFThalf);

// Sloping hearing loss gains
std::vector<float> HLgains(N);
std::vector<float> HL_None(N);
std::vector<float> HL_N2(N);
std::vector<float> HL_N4(N);
std::vector<float> HL_N6(N);
std::vector<float> HL_S1(N);
std::vector<float> HL_S2(N);
std::vector<float> HL_S3(N);


bool setup(BelaContext *context, void *userData)
{
	rt_printf("HLS setup starting...\n");
	
	// Create lookup table of HL audiograms
	HL_None = HLaudiogram(freqs, "None");
	HL_N2 = HLaudiogram(freqs, "N2");
	HL_N4 = HLaudiogram(freqs, "N4");
	HL_N6 = HLaudiogram(freqs, "N6");
	HL_S1 = HLaudiogram(freqs, "S1");
	HL_S2 = HLaudiogram(freqs, "S2");
	HL_S3 = HLaudiogram(freqs, "S3");

	// Convert HL gains to linear scale
	for (int i = 0; i < N; i++) {
		HL_None[i] = pow(10.0, -HL_None[i] / 20.0);
		HL_N2[i] = pow(10.0, -HL_N2[i] / 20.0);
		HL_N4[i] = pow(10.0, -HL_N4[i] / 20.0);
		HL_N6[i] = pow(10.0, -HL_N6[i] / 20.0);
		HL_S1[i] = pow(10.0, -HL_S1[i] / 20.0);
		HL_S2[i] = pow(10.0, -HL_S2[i] / 20.0);
		HL_S3[i] = pow(10.0, -HL_S3[i] / 20.0);
	}
	
	// Smearing kernel database
	std::vector<float> kernel;
	std::vector<std::vector<float>> kernelCoeffs;
	float smearingFreq;
	// B = 1
	for (int i = 0; i < FFThalf; i++) {
		smearingFreq = (float)i / (float)FFThalf * (float)context->audioSampleRate/ds / 2;
		// Broadened GT
		kernelCoeffs = gammaTone(smearingFreq, context->audioSampleRate/ds, b1*1.0);
		kernel = auditoryFilter(kernelCoeffs, context->audioSampleRate/ds, FFThalf);
		// Kernel integral for normalization
		float ksum = 0;
		for (int j = 0; j < FFThalf; j++) {
			ksum += kernel[j];
		}
		
		for (int j = 0; j < FFThalf; j++) {
			A_1(i, j) = kernel[j] / ksum;
		}
	}
	
	// B = 3
	for (int i = 0; i < FFThalf; i++) {
		smearingFreq = (float)i / (float)FFThalf * (float)context->audioSampleRate/ds / 2;
		// Broadened GT
		kernelCoeffs = gammaTone(smearingFreq, context->audioSampleRate/ds, b1*3.0);
		kernel = auditoryFilter(kernelCoeffs, context->audioSampleRate/ds, FFThalf);
		// Kernel integral for normalization
		float ksum = 0;
		for (int j = 0; j < FFThalf; j++) {
			ksum += kernel[j];
		}
		
		for (int j = 0; j < FFThalf; j++) {
			A_3(i, j) = kernel[j] / ksum;
		}
	}
	
	// B = 6
	for (int i = 0; i < FFThalf; i++) {
		smearingFreq = (float)i / (float)FFThalf * (float)context->audioSampleRate/ds / 2;
		// Broadened GT
		kernelCoeffs = gammaTone(smearingFreq, context->audioSampleRate/ds, b1*6.0);
		kernel = auditoryFilter(kernelCoeffs, context->audioSampleRate/ds, FFThalf);
		// Kernel integral for normalization
		float ksum = 0;
		for (int j = 0; j < FFThalf; j++) {
			ksum += kernel[j];
		}
		
		for (int j = 0; j < FFThalf; j++) {
			A_6(i, j) = kernel[j] / ksum;
		}
	}
	
	// GUI
    gui.setup(context->projectName);
    controller.setup(&gui, "Controls");
    outGain = controller.addSlider("Output gain [dB]", 0, -25, 50, 1);
    inpGain = controller.addSlider("Input gain [dB]", 0, -25, 50, 1);
    inpCal = controller.addSlider("Calibration offset [dB]", 0, -150, 150, 1);
    SmearingB = controller.addSlider("Smearing Broadening factor", 0, 0, 6, 1);
    HLType = controller.addSlider("HL audiogram type", 0, 0, 6, 1);
    invCompTrig = controller.addSlider("Toggle inverse compression", 0, 0, 1, 1);

	// Scope
	Scope setup;
	scope.setup(2, context->audioSampleRate/ds);

	
	// Bela parameters
	fs = context->audioSampleRate; // true bela sampling rate    
    
	// Buffer resizing
	blockSize = context->audioFrames/ds;
	FIFOout.resize(blockSize, std::vector<float>(N));
	FIFOin.resize(blockSize, std::vector<float>(N));
	s1.resize(blockSize, std::vector<float>(N));
	s2.resize(blockSize, std::vector<float>(N));
	gtOut.resize(blockSize, std::vector<float>(N));
	dhpaf0Out.resize(blockSize, std::vector<float>(N));
	dhpaf1Out.resize(blockSize, std::vector<float>(N));
	invdhpaf0Out.resize(blockSize, std::vector<float>(N));
	invdhpaf1Out.resize(blockSize, std::vector<float>(N));
	invComp0Out.resize(blockSize, std::vector<float>(N));
	invComp1Out.resize(blockSize, std::vector<float>(N));
	chain0Out.resize(blockSize, std::vector<float>(N));
	chain1Out.resize(blockSize, std::vector<float>(N));
	invlpaf0Out.resize(blockSize, std::vector<float>(N));
	invlpaf1Out.resize(blockSize, std::vector<float>(N));
	audioOut.resize(blockSize, 0.0);
	inpBuf.resize(blockSize, 0.0);
	oaOut.resize(blockSize, 0.0);
	fftinp_0.resize(blockSize*ZP, 0.0);
	fftinp_1.resize(blockSize*ZP, 0.0);
	fftout_0.resize(blockSize*ZP, 0.0);
	fftout_1.resize(blockSize*ZP, 0.0);
	fft_oaBuf.resize(blockSize/2, 0.0);
	oa_Buf.resize(blockSize/2, 0.0);

	
	
	// Hann Window
	hannW = hann(blockSize*2);
	// square root han
	fftHann = hann(blockSize);
	for (int i = 0; i < blockSize; i++) {
		fftHann[i] = sqrt(fftHann[i]);
	}
	
	// Compute group delay for each GT filter and resize synthesis FIFO accordingly
	float grpDel;
	int nDelay;
	int Dmax = (int)(groupDelay(freqs[0])*fs/ds + 1.5); // Max delay
	for (int i = 0; i < N; i++) {
		grpDel = groupDelay(freqs[i]);

		// Round to integer number of samples
		nDelay = Dmax - (int)(grpDel*fs/ds + 0.5);
		SynthFIFO[i].resize(nDelay);
	}

	// Make list of power levels
	for (int i = 0; i < Nlevels; i++) {
		levels[i] = i*1.0;
	}
	
	// Populate hpafDatabase for each channel and level
	for (int i = 0; i < Nlevels; i++) {
		for (int j = 0; j < N; j++) {
			float frat = 0.466 + 0.0109*levels[i];
			hpafDatabase[i][j] = AF(freqs[j]*frat, fs/ds, b2, c2);
		}
	}
	
	// Build filterbanks and dhpaf database
	std::vector<std::vector<std::vector<float>>> gtCoeffs(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	std::vector<std::vector<std::vector<float>>> lpafCoeffs(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	std::vector<std::vector<std::vector<float>>> dhpafCoeffs(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	std::vector<std::vector<std::vector<float>>> shpafCoeffs(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	std::vector<std::vector<std::vector<float>>> invlpafCoeffs(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	// pGC
	for (int i = 0; i < N; i++) {
		gtCoeffs[i] = gammaTone(freqs[i], fs/ds, b1);
		lpafCoeffs[i] = AF(freqs[i], fs/ds, b1, c1);
		// Make inverse LPAF coeffs, normalized w.r.t. b0 (new a0)
		for (int j = 0; j < 4; j++) {
			invlpafCoeffs[i][j][0] = lpafCoeffs[i][j][3] / lpafCoeffs[i][j][0];
			invlpafCoeffs[i][j][1] = lpafCoeffs[i][j][4] / lpafCoeffs[i][j][0];
			invlpafCoeffs[i][j][2] = lpafCoeffs[i][j][5] / lpafCoeffs[i][j][0];
			invlpafCoeffs[i][j][3] = lpafCoeffs[i][j][0] / lpafCoeffs[i][j][0];
			invlpafCoeffs[i][j][4] = lpafCoeffs[i][j][1] / lpafCoeffs[i][j][0];
			invlpafCoeffs[i][j][5] = lpafCoeffs[i][j][2] / lpafCoeffs[i][j][0];
		}
	}
	gtFB = filterbank(N, gtCoeffs);
	lpafFB = filterbank(N, lpafCoeffs);
	invlpafFB_0 = filterbank(N, invlpafCoeffs);
	invlpafFB_1 = filterbank(N, invlpafCoeffs);

	
	// Normalize pGC
	float pgcGain;
	std::vector<SOS> sosVec(2);
	for (int i = 0; i < N; i++) {
		sosVec[0] = gtFB.bank[i / 4];
		sosVec[1] = lpafFB.bank[i / 4];
		pgcGain = SOSgain(sosVec, freqs[i], fs/ds, (i % 4));
		// Apply normalization
		gtCoeffs[i][0][0] /= pgcGain;
		gtCoeffs[i][0][1] /= pgcGain;
		gtCoeffs[i][0][2] /= pgcGain;
	}
	gtFB = filterbank(N, gtCoeffs);
	

	// Build HPAF FB of max power
	for (int i = 0; i < N; i++) {
		dhpafCoeffs[i] = hpafDatabase[Nlevels - 1][i];
	}
	dhpafFB_0 = filterbank(N, dhpafCoeffs);
	
	// Normalize HPAF databse w.r.t. max power
	sosVec = std::vector<SOS>(3);
	float dcgcGain;
	for (int i = 0; i < N; i++) {
		sosVec[0] = gtFB.bank[i / 4];
		sosVec[1] = lpafFB.bank[i / 4];
		sosVec[2] = dhpafFB_0.bank[i / 4];
		dcgcGain = SOSgain(sosVec, freqs[i], fs/ds, i % 4);
		for (int j = 0; j < Nlevels; j++) {
			hpafDatabase[j][i][0][0] /= dcgcGain;
			hpafDatabase[j][i][0][1] /= dcgcGain;
			hpafDatabase[j][i][0][2] /= dcgcGain;
		}
		dhpafCoeffs[i] = hpafDatabase[Nlevels-1][i];
		// Make normalized shpaf filter
		shpafCoeffs[i] = AF(freqs[i]*fratL, fs/ds, b2, c2);
		shpafCoeffs[i][0][0] /= dcgcGain;
		shpafCoeffs[i][0][1] /= dcgcGain;
		shpafCoeffs[i][0][2] /= dcgcGain;
	}
	shpafFB = filterbank(N, shpafCoeffs);
	dhpafFB_0 = filterbank(N, dhpafCoeffs);
	dhpafFB_1 = filterbank(N, dhpafCoeffs);
	invdhpafFB_0 = filterbank(N, dhpafCoeffs);
	invdhpafFB_1 = filterbank(N, dhpafCoeffs);
	
	// Build inverse HPAF database
	for (int i = 0; i < Nlevels; i++) {
		for (int j = 0; j < N; j++) {
			for (int k = 0; k < 4; k++) {
				// Rotate and normalize
				invhpafDatabase[i][j][k][0] = hpafDatabase[i][j][k][3] / hpafDatabase[i][j][k][0];
				invhpafDatabase[i][j][k][1] = hpafDatabase[i][j][k][4] / hpafDatabase[i][j][k][0];
				invhpafDatabase[i][j][k][2] = hpafDatabase[i][j][k][5] / hpafDatabase[i][j][k][0];
				invhpafDatabase[i][j][k][3] = hpafDatabase[i][j][k][0] / hpafDatabase[i][j][k][0];
				invhpafDatabase[i][j][k][4] = hpafDatabase[i][j][k][1] / hpafDatabase[i][j][k][0];
				invhpafDatabase[i][j][k][5] = hpafDatabase[i][j][k][2] / hpafDatabase[i][j][k][0];
			}
		}
	}
	
	// Level estimators for each channel
	estimators = std::vector<levelEstimator>(N, levelEstimator(C, fs/context->audioFrames)); 
	
	// Build inverse compression filterbank
	for (int i = 0; i < Nlevels; i++) {
		for (int j = 0; j < N; j++) {
			float frat = levels[Nlevels-1]*0.0109 + 0.466 - 0.0109*levels[i];
			invCompDatabase[i][j] = AF(freqs[j]*frat, fs/ds, b2, c2);
		}
	}
	
	// Build inv comp FB of max power
	std::vector<std::vector<std::vector<float>>> invCompCoeffs(N, std::vector<std::vector<float>>(4, std::vector<float>(6, 0.0)));
	for (int i = 0; i < N; i++) {
		invCompCoeffs[i] = invCompDatabase[Nlevels - 1][i];
	}
	invCompFB_0 = filterbank(N, invCompCoeffs);
	
	// Normalize inv comp FB w.r.t max power

	float invCompGain;
	for (int i = 0; i < N; i++) {
		sosVec[0] = gtFB.bank[i / 4];
		sosVec[1] = lpafFB.bank[i / 4];
		sosVec[2] = invCompFB_0.bank[i / 4];
		invCompGain = SOSgain(sosVec, freqs[i], fs/ds, i % 4);
		for (int j = 0; j < Nlevels; j++) {
			invCompDatabase[j][i][0][0] /= invCompGain;
			invCompDatabase[j][i][0][1] /= invCompGain;
			invCompDatabase[j][i][0][2] /= invCompGain;
		}
		invCompCoeffs[i] = invCompDatabase[Nlevels-1][i];
	}
	invCompFB_0 = filterbank(N, invCompCoeffs);
	invCompFB_1 = filterbank(N, invCompCoeffs);
	
	// Find GT gains to normalize complete system
	std::vector<SOS> gtVec(1);
	std::vector<float> gtGains(N);
	for (int i = 0; i < N; i++) {
		gtVec[0] = gtFB.bank[i / 4];
		gtGains[i] = SOSgain(gtVec, freqs[i], fs/ds, (i % 4));
	}
	// Apply this normalization to HL audiograms
	for (int i = 0; i < N; i++) {
		HL_None[i] /= gtGains[i];
		HL_N2[i] /= gtGains[i];
		HL_N4[i] /= gtGains[i];
		HL_N6[i] /= gtGains[i];
		HL_S1[i] /= gtGains[i];
		HL_S2[i] /= gtGains[i];
		HL_S3[i] /= gtGains[i];
	}

	rt_printf("Setup done - now running.\n");

	return true;
}

void render(BelaContext *context, void *userData)
{

	// Read from GUI
	float tmpGain = controller.getSliderValue(outGain);	
	float tmpCal = controller.getSliderValue(inpCal);
	tmpInpGain = controller.getSliderValue(inpGain);
	int tmpB = controller.getSliderValue(SmearingB);
	int tmpInvCompTrig = controller.getSliderValue(invCompTrig);
	int tmpHLType = controller.getSliderValue(HLType);
	if (tmpHLType == 0) {HLgains = HL_None;}
	else if  (tmpHLType == 1) {HLgains = HL_N2;}
	else if  (tmpHLType == 2) {HLgains = HL_N4;}
	else if  (tmpHLType == 3) {HLgains = HL_N6;}
	else if  (tmpHLType == 4) {HLgains = HL_S1;}
	else if  (tmpHLType == 5) {HLgains = HL_S2;}
	else if  (tmpHLType == 6) {HLgains = HL_S3;}

	for (int i = 0; i < blockSize; i++) {
		// Build input signal
		inp = 0;
		inp += powf_neon(10, tmpInpGain / 20.0) * audioRead(context, i*ds, 0);
		inpBuf[i] = inp;
		
		// Filter signal through system
		signals = gtFB.process(inp);
		s1[i] = lpafFB.process(signals);
		s2[i] = shpafFB.process(s1[i]);
		
		// Branch 0
		dhpaf0Out[i]  = dhpafFB_0.process(s1[i]);
		invdhpaf0Out[i] = invdhpafFB_0.process(dhpaf0Out[i]);
		// Toggle inverse compression
		if (tmpInvCompTrig == 0) {
			signals = invdhpaf0Out[i];
		}
		else {
			invComp0Out[i] = invCompFB_0.process(invdhpaf0Out[i]);
			signals = invComp0Out[i];
		}
		invlpaf0Out[i] = invlpafFB_0.process(signals);

		// Branch 1
		dhpaf1Out[i]  = dhpafFB_1.process(s1[i]);
		invdhpaf1Out[i] = invdhpafFB_1.process(dhpaf1Out[i]);
		// Toggle inverse compression
		if (tmpInvCompTrig == 0) {
			signals = invdhpaf1Out[i];
		}
		else {
			invComp1Out[i] = invCompFB_1.process(invdhpaf1Out[i]);
			signals = invComp1Out[i];
		}
		invlpaf1Out[i] = invlpafFB_1.process(signals);
	}
	

	// Overlap-add, delay compensation and filterbank summation and apply hearing loss
	chain0Out = invlpaf0Out;
	chain1Out = invlpaf1Out;
	for (int i = 0; i < blockSize; i++) {
		audioOut[i] = 0;
		for (int j = 0; j < N; j++) {
			FIFOin[i][j] = hannW[i]*chain0Out[i][j] + hannW[i + blockSize]*chain1Out[i][j];
			FIFOout[i][j] = SynthFIFO[j].process(FIFOin[i][j]);
			audioOut[i] += FIFOout[i][j] * HLgains[j];
		}
	}
	
	
	
	// Fill up FFT input buffers
	for (int i = 0; i < blockSize/2; i++) {
		fftinp_1[i] = fftHann[i] * oa_Buf[i];
		fftinp_1[i + blockSize/2] = audioOut[i] * fftHann[i + blockSize/2];
		fftinp_0[i] = audioOut[i] * fftHann[i];
		fftinp_0[i + blockSize/2] = audioOut[i + blockSize/2] * fftHann[i + blockSize/2];
		oa_Buf[i] = audioOut[i + blockSize/2];
	}
	
	
	// FFT

	fft_0.fft(fftinp_0);
	fft_1.fft(fftinp_1); 
	
	// Retrieve Magnitude spectrum
	std::vector<float> S_0(FFThalf);
	std::vector<float> S_1(FFThalf);
	for (int i = 0; i < (FFThalf); i++) {
		S_0[i] = fft_0.fda(i) * fft_0.fda(i);
		S_1[i] = fft_1.fda(i) * fft_1.fda(i);
	}
	
	// Retrieve phase
	std::vector<float> phi_0(FFThalf);
	std::vector<float> phi_1(FFThalf);
	for (int i = 0; i < (FFThalf); i++) {
		phi_0[i] = fft_0.fdp(i);
		phi_1[i] = fft_1.fdp(i);
	}
	
	// Apply Smearing matrix to magnitude spectrum
	v_0 = Eigen::Map<Eigen::VectorXf>(S_0.data(), FFThalf);
	v_1 = Eigen::Map<Eigen::VectorXf>(S_1.data(), FFThalf);
	if (tmpB == 1) {
		out_0 = A_1*v_0;
		out_1 = A_1*v_1;
	}
	else if (tmpB == 3) {
		out_0 = A_3*v_0;
		out_1 = A_3*v_1;
	}
	else if (tmpB == 6) {
		out_0 = A_6*v_0;
		out_1 = A_6*v_1;
	}
	else {
		out_0 = v_0;
		out_1 = v_1;
	}

	std::vector<float> S_0smeared = std::vector<float>(out_0.data(), out_0.data() + out_0.size());
	std::vector<float> S_1smeared = std::vector<float>(out_1.data(), out_1.data() + out_1.size());
	
	// Recombine with phase to generate euclidian spectrum input
	for (int i = 0; i < FFThalf; i++) {
		fft_0.frequencyDomain[i].r = sqrtf_neon(S_0smeared[i]) * cosf_neon(phi_0[i]);
		fft_0.frequencyDomain[i].i = sqrtf_neon(S_0smeared[i]) * sinf_neon(phi_0[i]);
		fft_1.frequencyDomain[i].r = sqrtf_neon(S_1smeared[i]) * cosf_neon(phi_1[i]);
		fft_1.frequencyDomain[i].i = sqrtf_neon(S_1smeared[i]) * sinf_neon(phi_1[i]);
	}
	
	
	
	// Perform IFFT
	fft_0.ifft(); 
	fft_1.ifft();
	
	// Retrieve time domain representation
	for (int i = 0; i < blockSize; i++) {
		fftout_0[i] = fft_0.td(i);
		fftout_1[i] = fft_1.td(i);
	}

	// Do overlap-add
	for (int i = 0; i < blockSize/2; i++) {
		oaOut[i] = fftout_1[i]*fftHann[i] + fft_oaBuf[i]*fftHann[i + blockSize/2];
		oaOut[i + blockSize/2] = fftout_1[i + blockSize/2]*fftHann[i + blockSize/2] + fftout_0[i]*fftHann[i];
		fft_oaBuf[i] = fftout_0[i + blockSize/2];
	}
	
	// Write to audio out and scope
	for (int i = 0; i < blockSize; i++) {

		// Write to audio out
		audioWrite(context, i*ds, 0, oaOut[i] * powf_neon(10, tmpGain / 20.0));
		audioWrite(context, i*ds, 1, oaOut[i] * powf_neon(10, tmpGain / 20.0));

		// Write to scope
		scope.log(inpBuf[i], oaOut[i]);
		
	}

	// Calculate signal RMS
	for (int i = 0; i < N; i++) {
		s1rms[i] = 0.;
		s2rms[i] = 0.;
		for (int j = 0; j < blockSize; j++) {
			s1rms[i] += s1[j][i] * s1[j][i];
			s2rms[i] += s2[j][i] * s2[j][i];
		}
		
		s1rms[i] = sqrtf_neon(s1rms[i] / (float)(blockSize));
		s2rms[i] = sqrtf_neon(s2rms[i] / (float)(blockSize));
	}
	
	// Level estimation once per audio block and update dhpafFB coefficients accordingly
	for (int i = 0; i < N/4; i++) {
		for (int j = 0; j < 4; j++) {
			// Run level estimator for channel i
			Pc = estimators[i*4 + j].estLin(s1rms[i*4 + j]) + tmpCal;

			// Save current power
			PcMem[i*4 + j] = Pc;
			// Find pre-computed dHPAF coefficients for the closest vlaue of Pc
			idx = findClosest(levels, Pc);
			newCoeffs[j] = hpafDatabase[idx][i*4 + j];
			newInvCoeffs[j] = invhpafDatabase[idx][i*4 + j];
			newInvCompCoeffs[j] = invCompDatabase[idx][i*4 + j];
		}
		
		// Update dHPAF corresponding to switch state
		if (oa_switch) {
			dhpafFB_0.bank[i].updateCoeffs(newCoeffs);
			invdhpafFB_0.bank[i].updateCoeffs(newInvCoeffs);
			invCompFB_0.bank[i].updateCoeffs(newInvCompCoeffs);
		}
		else {
			dhpafFB_1.bank[i].updateCoeffs(newCoeffs);
			invdhpafFB_1.bank[i].updateCoeffs(newInvCoeffs);
			invCompFB_1.bank[i].updateCoeffs(newInvCompCoeffs);
		}
		
	}
	
	// Flip overlap-add switch
	oa_switch = !oa_switch;
	
	// Rotate hann window
	std::rotate(hannW.begin(), hannW.begin() + blockSize, hannW.end());
	
	// Count number of passed audio blocks
	trig += 1;
	
	// UNCOMMENT FOLLOWING BLOCK FOR CALIBRATION:
	// Print power levels once per second
	//if ((trig % (int)(fs/context->audioFrames)) == 0) {
	//	for (int i = 0; i < N; i++) {
	//		rt_printf("%.f  ", PcMem[i]);
	//	}
	//	rt_printf("\n");
	//}
}

void cleanup(BelaContext *context, void *userData)
{
	
}