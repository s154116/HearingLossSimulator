#include <vector>

/// Frequency to ERB scale mapping.
///
/// Returns a list of frequencies spaced equally on an ERB scale. Based on *Glasberg, B. R., & Moore, B. C. J. (1990). Derivation of auditory filter shapes from notched-noise data. Hearing Research, 47(1–2), 103–138. https://doi.org/10.1016/0378-5955(90)90170-T*.
/// @param f_low Minimum frequency [Hz].
/// @param f_high Maximum frequency [Hz].
/// @param N Number of desired frequencies.
/// @returns Vector containing frequencies.
std::vector<float> erbSpace(int f_low, int f_high, int N);