/// Level Estimator.
///
/// Class for level estimation component. Uses both passive gammachirp output *s1* and slightly compressed HPAF output *s2* with fixed *frat* = 1.08.
/// Utilizes instantaneous growth and exponential decay. Can be calibrated via *C*.
/// Based on *Irino, T., & Patterson, R. D. (2006). A dynamic compressive gammachirp auditory filterbank. IEEE Transactions on Audio, Speech and Language Processing, 14(6), 2222–2232. https://doi.org/10.1109/TASL.2006.874669*.

class levelEstimator {
	public:
		/// Default constructor.
		levelEstimator();
		/// Constructor.
		/// @param C Calibration parameter [dB].
		/// @param fs Sampling frequency of system [Hz].
		levelEstimator(float C, float fs);
		/// Estimation function.
		/// @param s1 pGC output.
		/// @param Fixed HPAF output.
		/// @returns Floating point representation of instantaneous power.
		float est(float s1, float s2);
		/// Linear estimation function.
		/// This is a type of instantaneous-reacting and exponential-decaying level estimation algorithm.
		/// @param s Input signal strength. Either RMS or peak, depending on calibration method.
		/// @returns Floating point representation of instantaneous power.
		float estLin(float s);
	private:
		float wL;
		float aRL;
		float tauL;
		float v1L;
		float v2L;
		float PRL;
		/// Memory block for *s1*.
		float zs1;
		/// Memory block for *s2*.
		float zs2;
		// Memory block for linear estimation
		float zs;
		float fs;
};