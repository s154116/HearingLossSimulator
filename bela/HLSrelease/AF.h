#include <vector>

/// Generates biquad filter coefficients.
///
/// This function calculates a set of coefficients for four digital IIR biquad filters to be used in series. The filter characteristic is either lowpass or highpass, depending on the sign of *c* and is used in gammachirp auditory models.
/// Code is adapted from brian.hears python package.
/// Model is from *Unoki, M., Irino, T., & Patterson, R. D. (2001). Improvement of an IIR asymmetric compensation gammachirp filter. Acoustical Science and Technology, 22(6), 426–430. https://doi.org/10.1250/ast.22.426*.
/// @param[in] fc Center frequency (point of symmetry).
/// @param[in] fs Sampling frequency of system, in Hertz.
/// @param[in] b Envelope bandwidth parameter.
/// @param[in] c Chirp factor. If *c* is positive, filter will be highpass, if negative it will be lowpass.
/// @param[out] Output is [4x6] two-dimensional vector.

std::vector<std::vector<float>> AF(float fc, float fs, float b, float c);