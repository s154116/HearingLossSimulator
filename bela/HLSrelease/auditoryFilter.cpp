/***** auditoryFilter.cpp *****/

#include <auditoryFilter.h>
#include <complex>

std::vector<float> auditoryFilter(std::vector<std::vector<float>> coeffs, float fs, int n) {
	
	// Vector of sampling frequencies
	std::vector<std::complex<double>> f(n);
	for (int i = 0; i < n; i++) {
		f[i] = std::complex<double>(-(float)i/ (float)(n - 1), 0.0);
	}
	
	// Find gain of biquad at that frequency
	std::complex<double> tmpGain;
	// Complex unit
	std::complex<double> I(0.0, 1.0);
	// pi
	std::complex<double> pi((double)M_PI, 0.0);
	std::vector<std::complex<double>> gainList(n, std::complex<float>(0.0, 0.0));
	std::vector<float> gainListf(n, 0.0);
	
	// Initialize complex coefficients
	std::complex<double> b00;
	std::complex<double> b01;
	std::complex<double> b02;
	std::complex<double> b10;
	std::complex<double> b11;
	std::complex<double> b12;
	std::complex<double> b20;
	std::complex<double> b21;
	std::complex<double> b22;
	std::complex<double> b30;
	std::complex<double> b31;
	std::complex<double> b32;
	std::complex<double> a00;
	std::complex<double> a01;
	std::complex<double> a02;
	std::complex<double> a10;
	std::complex<double> a11;
	std::complex<double> a12;
	std::complex<double> a20;
	std::complex<double> a21;
	std::complex<double> a22;
	std::complex<double> a30;
	std::complex<double> a31;
	std::complex<double> a32;
	
	// Cast biquad coefficients to complex
	b00 = std::complex<double>(coeffs[0][0], 0.0);
	b01 = std::complex<double>(coeffs[0][1] ,0.0);
	b02 = std::complex<double>(coeffs[0][2], 0.0);
	b10 = std::complex<double>(coeffs[1][0], 0.0);
	b11 = std::complex<double>(coeffs[1][1], 0.0);
	b12 = std::complex<double>(coeffs[1][2], 0.0);
	b20 = std::complex<double>(coeffs[2][0], 0.0);
	b21 = std::complex<double>(coeffs[2][1], 0.0);
	b22 = std::complex<double>(coeffs[2][2], 0.0);
	b30 = std::complex<double>(coeffs[3][0], 0.0);
	b31 = std::complex<double>(coeffs[3][1], 0.0);
	b32 = std::complex<double>(coeffs[3][2], 0.0);
	
	a00 = std::complex<double>(coeffs[0][3], 0.0);
	a01 = std::complex<double>(coeffs[0][4], 0.0);
	a02 = std::complex<double>(coeffs[0][5], 0.0);
	a10 = std::complex<double>(coeffs[1][3], 0.0);
	a11 = std::complex<double>(coeffs[1][4], 0.0);
	a12 = std::complex<double>(coeffs[1][5], 0.0);
	a20 = std::complex<double>(coeffs[2][3], 0.0);
	a21 = std::complex<double>(coeffs[2][4], 0.0);
	a22 = std::complex<double>(coeffs[2][5], 0.0);
	a30 = std::complex<double>(coeffs[3][3], 0.0);
	a31 = std::complex<double>(coeffs[3][4], 0.0);
	a32 = std::complex<double>(coeffs[3][5], 0.0);

	// Calculate gain at each frequency
	for (int j = 0; j < n; j++) {
		tmpGain = ((b00 + b01*exp(pi*I*f[j]) + b02*exp(pi*I*f[j]*2.))
				*pow(a00 + a01*exp(pi*I*f[j]) + a02*exp(pi*I*f[j]*2.), -1.0)
					
			*  (b10 + b11*exp(pi*I*f[j]) + b12*exp(pi*I*f[j]*2.))
				*pow(a10 + a11*exp(pi*I*f[j]) + a12*exp(pi*I*f[j]*2.), -1.0)
					
			*  (b20 + b21*exp(pi*I*f[j]) + b22*exp(pi*I*f[j]*2.))
				*pow(a20 + a21*exp(pi*I*f[j]) + a22*exp(pi*I*f[j]*2.), -1.0)
					
			*  (b30 + b31*exp(pi*I*f[j]) + b32*exp(pi*I*f[j]*2.))
				*pow(a30 + a31*exp(pi*I*f[j]) + a32*exp(pi*I*f[j]*2.), -1.0));

		gainList[j] = tmpGain;
	}
	
	// Cast to float
	for (int i = 0; i < n; i++) {
		gainListf[i] = fabs(gainList[i]);
	}
	
	return gainListf;
	
}