#include <libraries/ne10/NE10.h>
#include <vector>
#include <cmath>
#include <libraries/math_neon/math_neon.h>

/// FFT object
///
/// Wrapper for [NE10](http://projectne10.github.io/Ne10/doc/annotated.html), adapted from [bela] (https://github.com/BelaPlatform/Bela/blob/master/libraries/Fft/Fft.h).
/// Changes to this version include method to retrieve phase information and to perform IFFT directly on an input vector.
/// Assumes real time signal and thus symmetric spectrum.
/// Uses Radix-2 implementation and FFT lengths must thus be a power of two.

class Fft
{
public:
	/// Default Constructor
	Fft(){};
	/// Constructor
	///
	/// @param length FFT size.
	Fft(unsigned int length){ setup(length); };
	/// Destrcutor
	~Fft(){ cleanup(); };
	/// Initialize FFT length
	int setup(unsigned int length);
	void cleanup();
	
	/// Perform FFT calculation on internal time domain representation.
	void fft();

	/// Perform FFT calculation on input vector.
	///
	/// @param input Time domain input vector.
	void fft(const std::vector<float>& input);
	/// Perform IFFT on internal frequency domain representation.
	void ifft();
	/// Perform IFFT on input vector.
	///
	/// @param reInput Real spectrum vector.
	/// @param imInput Imaginary spectrum vector.
	void ifft(const std::vector<float>& reInput, const std::vector<float>& imInput);

	/// Retrieve real frequency domain representation.
	/// 
	/// @param n Frequency bin.
	/// @returns Real frequency domain representation.
	float& fdr(unsigned int n) { return frequencyDomain[n].r; };
	
	/// Retrieve imaginary frequency domain representation.
	/// 
	/// @param n Frequency bin.
	/// @returns Imaginary frequency domain representation.
	float& fdi(unsigned int n) { return frequencyDomain[n].i; };

	/// Retrieve Magnitude frequency domain representation.
	/// 
	/// @param n Frequency bin.
	/// @returns Magnitude frequency domain representation.
	float fda(unsigned int n) { return sqrtf_neon(fdr(n) * fdr(n) + fdi(n) * fdi(n)); };
	
	/// Retrieve phase frequency domain representation.
	/// 
	/// @param n Frequency bin.
	/// @returns Phase frequency domain representation.
	float fdp(unsigned int n) {return atan2f(fdi(n), fdr(n)); };

	/// Retrieve Retrieve time domain representation.
	/// 
	/// @param n Time index.
	/// @returns Time domain representation.
	float& td(unsigned int n) { return timeDomain[n]; };
	/// Check if FFT length is power of two.
	///
	/// @param n Number to check.
	/// @returns True or false.
	static bool isPowerOfTwo(unsigned int n);
	/// Rounds up to nearest power of two.
	static unsigned int roundUpToPowerOfTwo(unsigned int n);
	
	ne10_float32_t* timeDomain = nullptr;
	ne10_fft_cpx_float32_t* frequencyDomain = nullptr;
	ne10_fft_r2c_cfg_float32_t cfg = nullptr;
	unsigned int length;
private:

};