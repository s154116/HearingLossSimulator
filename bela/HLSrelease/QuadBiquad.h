#pragma once
#include <array>
#include <arm_neon.h>
#include <new>
#include <stdlib.h>
#include <vector>

/// QuadBiquad
///
/// Coefficients of a single direct form II type IIR Biquad filter.

class BiquadCoeff {
	public:
		float b0;
		float b1;
		float b2;
		float a0;
		float a1;
		float a2;
		BiquadCoeff() {
			b0 = 1;
			b1 = 0;
			b2 = 0;
			a0 = 1;
			a1 = 0;
			a2 = 0;	
		}
		/// Constructor
		///
		/// @param initCoeffs Initial coefficients in object. 
		BiquadCoeff(std::vector<float> initCoeffs) {
			b0 = initCoeffs[0];
			b1 = initCoeffs[1];
			b2 = initCoeffs[2];
			a0 = initCoeffs[3];
			a1 = initCoeffs[4];
			a2 = initCoeffs[5];
		}
};

/// QuadBiquad
///
/// Class implementing NEON SIMD instruction to run four biquad filters in parallel. 
/// Adapted from [bela](https://github.com/BelaPlatform/Bela/tree/master/libraries/Biquad). 
/// @note For proper memory alignment, this object should not be in STL containers. Use dynamic memory with "new" instead.
/// @warning Uses NEON SIMD instructions. Must be used with compatible processor.

class QuadBiquad
{
public:
	/// Coefficients of four filters.
	std::array<BiquadCoeff, 4> filters;

	/// Constructor 
	///
	/// @note May throw exception if memory is not properly aligned in instatiation.
	QuadBiquad()
	{
		if(size_t(this) & size_t(alignof(QuadBiquad) - 1))
		{
			fprintf(stderr, "QuadBiquad object is improperly aligned. Avoid heap allocation, use operator new or use -std=c++17");
			std::bad_alloc e;
			throw(e);
		}
		
		// Initialize delay blocks
		float32_t data[4] = {0.0, 0.0, 0.0, 0.0};
		z1 = vld1q_f32(data);
		z2 = vld1q_f32(data);
	}

	
	void* operator new(size_t sz) {
		auto ptr = aligned_alloc(alignof(QuadBiquad), sz);
		if(!ptr)
		{
			std::bad_alloc e;
			throw(e);
		}
		return ptr;
	}
	
	/// Process filter one sample in time
	///
	/// @param data Sigle precision [4] dimensional array of input samples, one for each channel. Output is written onto same array.
	/// @note Input data is overwritten by output.
	
	void process(float data[4])
	{
		float32x4_t in = vld1q_f32(data);
	
		// A: out = in * a0 + z1;
		float32x4_t out = vmlaq_f32(z1, in, b0);
		// B: z1 = in * a1 + z2;
		z1 = vmlaq_f32(z2, in, b1);
		// D: z2 = in * a2;
		z2 = vmulq_f32(in, b2);
		// F: store the output
		vst1q_f32(data, out);
		// C: z1 = z1 - b1 * out; ***
		z1 = vmlaq_f32(z1, a1, out);
		// E: z2 = z2 - b2 * out; ***
		z2 = vmlaq_f32(z2, a2, out);
	
	}

	/// Refresh
	///
	/// Changes sign of a1 and a2 for optimized instructions.
	/// @note Does not alter coefficients. Should only be called once when updating coefficiets to avoid sign switches.
	void update(){
		#define QUADBIQUAD_UPDATE(arg,sign) \
		{ \
			float32_t data[4] = {sign float32_t(filters[0].arg), sign float32_t(filters[1].arg), sign float32_t(filters[2].arg), sign float32_t(filters[3].arg)}; \
			arg = vld1q_f32(data); \
		}
			QUADBIQUAD_UPDATE(b0,+);
			QUADBIQUAD_UPDATE(b1,+);
			QUADBIQUAD_UPDATE(b2,+);
			QUADBIQUAD_UPDATE(a1,-);
			QUADBIQUAD_UPDATE(a2,-);
			
	}



private:
	float32x4_t z1;
	float32x4_t z2;
	float32x4_t b0;
	float32x4_t b1;
	float32x4_t b2;
	float32x4_t a1;
	float32x4_t a2;
};

