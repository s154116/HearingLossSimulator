#include <SOS.h>

/// Prints Coefficients
///
/// Prints coefficients of four cascaded biquads. 
/// Output format is MATLAB readable.
/// @param filter SOS object
/// @param i SOS channel to print.

void printCoeffs(SOS filter, int i);