/***** printCoeffs.cpp *****/
#include <printCoeffs.h>
#include <Bela.h>

void printCoeffs(SOS filter, int i) {
	rt_printf("[");
	for (int j = 0; j < 4; j++) {
		rt_printf("%g, %g, %g, %g, %g, %g;\n", filter.QuadBiquadSeries[j]->filters[i].b0, filter.QuadBiquadSeries[j]->filters[i].b1, filter.QuadBiquadSeries[j]->filters[i].b2, filter.QuadBiquadSeries[j]->filters[i].a0, filter.QuadBiquadSeries[j]->filters[i].a1, filter.QuadBiquadSeries[j]->filters[i].a2);
	}
	rt_printf("];\n");
	return;
}	