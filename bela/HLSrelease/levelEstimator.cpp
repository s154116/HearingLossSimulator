/***** levelEstimator.cpp *****/

// Level estimation unit 
// Based on Irino & Patterson
// Delay block included for exponenttial decay - class implementation

#include <levelEstimator.h>
#include <cmath>
#include <libraries/math_neon/math_neon.h>


// Default constructor with no calibration
levelEstimator::levelEstimator() {
	// Default parameters
	wL = 0.5;
	tauL = 0.0005;
	v1L = 1.5,
	v2L = 0.5;
	PRL = 50; // arbitrary calibration
	aRL = pow(10, PRL/20.);
	zs1 = 0;
	zs2 = 0;
	zs = 0;
	fs = 44100;
}

// Constructor with calibration
levelEstimator::levelEstimator(float C, float sampleRate) {
	// Default parameters
	wL = 0.5;
	tauL = 0.0005;
	v1L = 1.5,
	v2L = 0.5;
	PRL = C; // Reference level [dB]
	aRL = pow(10, PRL/20.);
	zs1 = 0;
	zs2 = 0;
	zs = 0;
	fs = sampleRate;
}

// Process and update sound level
float levelEstimator::est(float s1, float s2) {
	float s1bar = fmax(zs1*expf_neon(-1*logf_neon(2.)/(tauL*fs)), fmax(s1, 0.));
	float s2bar = fmax(zs2*expf_neon(-1*logf_neon(2.)/(tauL*fs)), fmax(s2, 0.));
	
	// Update delay blocks
	zs1 = s1bar;
	zs2 = s2bar;
	
	return 20*log10f_neon(wL * aRL * powf(s1bar/aRL, v1L)  + (1 - wL) * aRL * powf_neon(s2bar/aRL, v2L) + 0.0001);
}

// Linear processing function
float levelEstimator::estLin(float s) {
	float sbar = fmax(zs*expf_neon(-1*logf_neon(2.)/(tauL*fs)), fmax(s, 0.));
	
	// Update delay blocks
	zs = sbar;
	
	return 20*log10f_neon(sbar) + PRL;
}



