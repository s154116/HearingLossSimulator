#include <string>
#include <vector>

/// Hearing Loss Audiogram
///
/// Calculates Hearing loss audiogramms of pre-set degrees. Based on data from N. Bisgaard, M. S. M. G. Vlaming and M. Dahlquist "Standard Audiograms for the IEC 60118-15 Measurement Procedure".
/// Uses linear interpolation for frequencies within data range and extrapolates boundary values for frequencies outside data range.
/// Returned values are given as losses in [dB] (conversely to gains). 
/// @param freqs Vector containing frequencies in [Hz] at which to sample audiogram.
/// @param type Hearing loss type. Must be either "N2" (mild flat), "N4" (moderate flat), "N6" (severe flat), "S1" (mild steep), "S2" (moderate steep) or "S3" (severe steep).
/// @returns Vector containing losses in [dB].

std::vector<float> HLaudiogram(std::vector<float> freqs, std::string type);