#include <vector>
#include <SOS.h>

/// SOS Gain
///
/// Calculates maximal gain of biquad filter series. Assumes known region of maximum, i.e. resonant frequency. 
/// @param filters Vector containing SOS objects.
/// @param fr Resonant frequency
/// @param fs System sampling frequency
/// @param n Channel in SOS.
/// @returns Gain of SOS system.

float SOSgain(std::vector<SOS> filters, float fr, float fs, int n);