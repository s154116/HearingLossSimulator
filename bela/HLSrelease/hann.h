#include <vector>

/// Hann function.
///
/// Calculates an N point hann window.
/// @param N Window length.
/// @returns Vector containing window values.

std::vector<float> hann(float N);