/***** groupDelay.cpp *****/

// Calculates delay of gammatone impulse reponse envelope peak
// Not true group delay, but rather average of gammatone filter
// By Youwei Yang, Yi Jiang, Runsheng Liu, Dongmei Li

// Takes center frequency of gammatone filter as input

#include <groupDelay.h>
#include <math.h>

float groupDelay(float fc) {
	float N = 4; // Order of gammatone filter
	// Compute factorial of N-1
	float Nfac = 1;
	for (int i = 1; i < N; i++) {
		Nfac *= i;
	}
	// Compute factorial of 2*N-2
	float N2fac = 1;
	for (int i = 1; i < (2*N-1); i++) {
		N2fac *= i;
	}
	//ERB at fc
	float erb = 24.7*(4.37/1000 * fc + 1);
	// Bandwidth
	float b = Nfac * Nfac / (M_PI * N2fac * std::pow(2, 2 - 2*N)) * erb;
	// Group Delay
	float tau = (N - 1) / (2*M_PI*b);
	return tau;
}





