# Simulating Hearing Impairment in Real-Time

A Master's Thesis by Tobias Kock (<s154116@student.dtu.dk>), DTU Electrical Engineering.  
Under supervision by Bastian Epp, DTU Health Tech.

For a showcase of audio sample files and a more detailed documentation of code see the project [webpage](http://www.student.dtu.dk/~s154116/index).

## Installation 
Requires bela board. Follow instructions at [bela tutorial](https://learn.bela.io/get-started-guide/setting-up-your-hardware/) to set up the board for the first time. It might be necessary to [flash an SD card](https://learn.bela.io/using-bela/bela-techniques/managing-your-sd-card/) with the latest image. The HLS project was developed using version 0.3.8b.

To run any development version of the HLS simply drag the [HLS.zip](bela/HLSrelease.zip) file into your local bela IDE. A dialog window will open, enter a project name and press *Confirm*.

Press the *Build & Run* IDE button and the program should start compiling. For safety reasons go through the calibration process before feeding bela output to speakers or headphones.

Note: The HLS uses the linear algebra library for C++ *Eigen* (http://eigen.tuxfamily.org). It comes pre-installed in the HLS software (version 3.3.9). It is published under the [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/FAQ/) license.

## Calibration

For a new setup the level estimators used in the non-linear system must be calibrated. To do this go through the following steps in `render.cpp` (see it [here](http://www.student.dtu.dk/~s154116/renderListing.html)):
 * Set analog sampling rate to 22050 Hz in the IDE.
 * Set audio frame length to 256 samples in the IDE.
 * Set number of channels `int N = 20;`
 * Set downsampling factor `int ds = 2;`
 * Set FFT length `int NFFT = 128;`
 * Set a reasonable frequency range in `int f_low` and `int f_high`, e.g. 100 Hz and 8000 Hz
 * Uncomment the designated print statements at the bottom of the program ([line 700 to 706](http://www.student.dtu.dk/~s154116/renderListing.html))
 * IMPORTANT: Unplug any speakers or headphones.
 * Press *Build & Run* button to compile program.

When the HLS is running the bela console should print 20 values continuously. This is the level estimate of each pGC filter in dB HL.

 * Feed a reference signal to bela audio input, either via an external device, a microphone or safe voltage source. Preferably a pure tone in the range 500 Hz - 1500 Hz.
 * Open the GUI.
 * Now adjust the sliders *Input Gain* and *Calibration Offset* in order to fit the bela printed level estimates to the reference level. *Input Gain* is in dB and attenuates/amplifies the input digitally via static multiplication. *Calibration offset* is an offset in the level estimators. Test that the level estimate is reasonable for the range 0 dB HL to 100 dB HL. If the noise level is too high lower *Input Gain* and increase *Calibration offset*. If the range of selectable *Calibration offset* is too narrow, adjust `int C` and recompile. `int C` is the default calibration which *Calibration offset* alters at run-time.
 * Note the selected values. The GUI is reset on a restart and must be configured again. Alternatively the found values can be hard-coded into `render.cpp`.
 * To calibrate the system output to match a subjects HL use appropriate audiological tests\* and adjust the bela output volume via *Output Gain* accordingly.
 * Repeat the above step for any new listener .
 * NOTE: There is no limiter on the output. Calibration and playback is done a own risk. Always start low and slowly increase volume.


\* This may vary depending on desired precision and ease of use. A simple threhsold detection at at single frequency is a rough, but not unreasonable approximation.

## Parameters
Any changes to parameters or system configuration must be performed in the `render.cpp` file. The most common settings are declared at the top of the program and changing the system itself must be done carefully throughout `render.cpp`. It is not recommended that you make changes to source files.

Below is a list of common settings:
 * Audio frame length in IDE. Has impact on performance, latency and spectral smearing kernel density. Recommended is 256 samples.
 * Sampling rate. This can be changed either via IDE *Analog Sampling Rate* or via downsampling factor `int ds`. Increasing sampling rate has a massive performance penalty and is not recommended. If `int ds` is changed FFT length must be changed to reflect this, i.e. `int NFFT = R;`, where `R` is audio frame length divided by downsampling factor.
 * Number of channels via `int N`. Must be an integer multiple of 4 due to filter processsing vectorisation. Recommended is 24, but depends on performance.
 * Desired frequency range via `f_min` and `f_max`. `f_max` should not violate Nyquists Theorem as no anti-aliasing filter is implemented. Recommended is `int f_min = 100;` and `int f_max = 8000;`.
 * For increased performance turn off "Use Analog", "Use Digital" and "Mute speaker" toggles, as well as reducing the number of "Digital Channels" to 2.
 * For increased performance pass `--high-performance-mode` to "User Command Line Arguments" field. This might yield IDE, console, GUI and scope unresponsive.
 * Using the scope causes major performance drop, especially when displaying long FFT spectra.

## Run-time Settings
When the system is running the various HL effects can be changed and combined via the GUI. See list below for how the settings are encoded in the sliders. It is not recommended to combine inverse compression and HL audiograms, since this leads to extreme attenuation.

+ `Smearing Broadening Factor`: Controls spectral smearing. If set to `1` the smearing width is equal to auditory filter width, if `3` or `6` it is broadened by a factor 3 or 6 respectively. Any other position of the slider will give no smearing effect.
+ `HL audiogram type`: Controls audiogram approximation. 
    + `0`: No applied audiogram.
    + `1`: Mild flat hearing loss (N_2).
    + `2`: Moderate flat hearing loss (N_4). May be inaudible.
    + `3`: Severe flat hearing loss (N_6). May be inaudible.
    + `4`: Mild steep hearing loss (S_1). 
    + `5`: Moderate steep hearing loss (S_2).
    + `6`: Severe steep hearing loss (S_3). May be inaudible.
+ `Toggle inverse compression`: On/Off (`1`/`0`) toggle of inverse compession. Necessitates calibration for effective simulation.

Additionally, the calibration, input gain and output gain (in decibel) can be changed at any point.
